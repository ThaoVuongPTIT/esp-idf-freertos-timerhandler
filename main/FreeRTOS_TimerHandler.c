/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#include "output.h"
#define BLINK_GPIO 2
TimerHandle_t vTask1;
TimerHandle_t vTask2;
void vTaskCallBack(TimerHandle_t xTime)
{
    int iID = (uint32_t) pvTimerGetTimerID(xTime);
    if(iID == 0)
    {
        printf("Vuong Viet Thao - B18DCDT237\n");
    }
    else if(iID == 1)
    {
        output_toggle(2);
    }
}
void app_main(void)
{
    output_create(2);
    vTask1 = xTimerCreate("Time1", 1000 / portTICK_RATE_MS, pdTRUE, (void *)0 ,vTaskCallBack);
    vTask2 = xTimerCreate("Time2", 2000 / portTICK_RATE_MS, pdTRUE, (void *)1, vTaskCallBack);
    xTimerStart(vTask1,0);
    xTimerStart(vTask2,0);
}
